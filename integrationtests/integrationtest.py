import requests
import os
"""
Set PORT, SERVER, PASSWORD environment variables
"""
PORT = os.environ.get("PORT",8000)
SERVER = os.environ.get("SERVER","http://localhost:{port}/").format(port=str(PORT))

USERNAME=os.environ.get("USERNAME")
PASSWORD=os.environ.get("PASSWORD")


#enter a log type
entrylogtype={
    "log_type":"usage"
}
r = requests.post(SERVER+"api/logtype/",auth=(USERNAME,PASSWORD),data=entrylogtype)
print(r.status_code)
print(r.text)

#enter a system type
entrysystem={
    "system_name":"windows"
}
r = requests.post(SERVER+"api/system/",auth=(USERNAME,PASSWORD),data=entrysystem)
print(r.status_code)
print(r.text)

#enter a log entry
entry = {
    "system_name":1,
    "log_type":1,
    "log_data":"this is a log file"
}
r = requests.post(SERVER+"api/log/",auth=(USERNAME,PASSWORD),data=entry)
print(r.status_code)
print(r.text)

#get request
r = requests.get(SERVER+"api/logtype/", auth=(USERNAME, PASSWORD))
print(r.status_code)
print(r.text)

