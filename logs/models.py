from django.db import models
from django.utils import timezone

# Create your models here.


class System(models.Model):
    system_name = models.CharField(max_length=200)
    date_added = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.system_name


class LogType(models.Model):
    log_type = models.CharField(max_length=200)
    date_added = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.log_type


class Log(models.Model):
    system_name = models.ForeignKey(System,on_delete=models.CASCADE)
    log_type = models.ForeignKey(LogType,on_delete=models.CASCADE)
    date_added = models.DateTimeField(default=timezone.now)
    log_data = models.TextField()

    def __str__(self):
        return self.system_name + " " + self.log_type + " " + self.log_data
