from rest_framework import serializers
from logs import models

class LogSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Log
        fields = ('id','system_name','log_type','date_added','log_data')
        read_only_fields=('date_added',)


class SystemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.System
        fields = ('id','system_name','date_added')
        read_only_fields=('date_added',)

class LogTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.LogType
        fields = ('id','log_type','date_added')
        read_only_fields=('date_added',)
