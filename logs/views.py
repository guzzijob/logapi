from django.shortcuts import render
from django.http import HttpResponse
from logs.serializers import LogSerializer,SystemSerializer,LogTypeSerializer
from rest_framework import viewsets
from logs.models import Log,LogType,System

# Create your views here.


def index(request):
    return HttpResponse("Hello, world.")


class LogViewSet(viewsets.ModelViewSet):
    serializer_class = LogSerializer
    queryset = Log.objects.all()


class SystemViewSet(viewsets.ModelViewSet):
    serializer_class = SystemSerializer
    queryset = System.objects.all()


class LogTypeViewSet(viewsets.ModelViewSet):
    serializer_class = LogTypeSerializer
    queryset = Log.objects.all()
