from django.urls import path,include
from rest_framework import routers
from rest_framework.schemas import get_schema_view
from rest_framework.documentation import include_docs_urls
from . import views
router = routers.DefaultRouter()
router.register('logtype',views.LogTypeViewSet)
router.register('system',views.SystemViewSet)
router.register('log',views.LogViewSet)
urlpatterns = [
    path('', include(router.urls)),
    path('index/',views.index,name="index"),
    path('schema/',get_schema_view('log_api')),
    path('docs/',include_docs_urls('log_api'))
]